<?php

namespace App\Http\Requests\Backend\Contact;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => [
                'required',
                'email',
                Rule::unique('contacts')->ignore($this->id)
            ],
            'organization.id' => 'required|exists:organizations,id',
            'mobiles.*.number' => 'required|digits:11',
            'birthDate' => 'required',
        ];
    }
}
