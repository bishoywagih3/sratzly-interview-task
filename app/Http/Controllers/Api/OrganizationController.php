<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrganizationResource;
use App\Http\Resources\OrganizationsCollection;
use App\Models\Organization\Organization;
use App\Repositories\Api\OrganizationRepository;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;

    public function __construct(OrganizationRepository $organizationRepository)
    {
        $this->organizationRepository = $organizationRepository;
    }

    public function index(Request $request)
    {
        $organizations = new OrganizationsCollection($this->organizationRepository->getPaginated($request->all()));

        return response()->json($organizations, 200);
    }
}
