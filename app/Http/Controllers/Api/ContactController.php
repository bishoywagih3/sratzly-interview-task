<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ContactsCollection;
use App\Repositories\Api\ContactRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    /**
     * @var ContactRepository
     */
    private $contactRepository;

    public function __construct(ContactRepository $contactRepository)
    {
        $this->contactRepository = $contactRepository;
    }

    public function index(Request $request)
    {
        $contacts = new ContactsCollection($this->contactRepository->getPaginated($request->all()));

        return response()->json($contacts, 200);
    }

    public function getFilteredWithOrganization(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'organization_id' => 'required|exists:organizations,id'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }

        $contacts = new ContactsCollection($this->contactRepository->getFilteredWithOrganization($request->all()));

        return response()->json($contacts, 200);
    }

    public function getFilteredWithEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }

        $contacts = new ContactsCollection($this->contactRepository->getFilteredWithEmail($request->all()));

        return response()->json($contacts, 200);
    }
}
