<?php

namespace App\Http\Controllers\Backend\Organization;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Organization\StoreOrganizationRequest;
use App\Http\Requests\Backend\Organization\UpdateOrganizationRequest;
use App\Models\Organization\Organization;
use App\Models\OrganizationCategory;
use App\Repositories\Backend\OrganizationRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OrganizationController extends Controller
{
    /**
     * @var OrganizationRepository
     */
    private $organizationRepository;

    public function __construct(OrganizationRepository $organizationRepository)
    {
        $this->organizationRepository = $organizationRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $organizations = $this->organizationRepository->getPaginated(10);

        return view('organizations.index', compact('organizations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        $categories = OrganizationCategory::pluck('name', 'name')
                        ->prepend('Select Category', '');

        return view('organizations.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreOrganizationRequest  $request
     * @return Response
     */
    public function store(StoreOrganizationRequest $request)
    {
        if ($request->hasFile('file')) {
            $request->validate([
                'file' => 'mimes:jpeg,jpg,png'
            ]);
            $request->file->store('organizations', 'public');
            $request['logo'] = $request->file->hashName();
        }

        $this->organizationRepository->store($request->except('file'));

        return back()->withFlashSuccess('Organization created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Organization\Organization  $organization
     * @return Response
     */
    public function edit(Organization $organization)
    {
        $categories = OrganizationCategory::pluck('name', 'name')
                        ->prepend('Select Category', '');

        return view('organizations.edit', compact('categories', 'organization'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Organization\Organization  $organization
     * @return Response
     */
    public function update(UpdateOrganizationRequest $request, Organization $organization)
    {
        if ($request->hasFile('file')) {
            $request->validate([
                'file' => 'mimes:jpeg,jpg,png'
            ]);
            $request->file->store('organizations', 'public');
            $request['logo'] = $request->file->hashName();
        }

        $this->organizationRepository->update($organization, $request->except('file'));

        return redirect()->route('organizations.index')
                         ->withFlashSuccess('Organization Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Organization\Organization  $organization
     * @return Response
     */
    public function destroy(Organization $organization)
    {
        $response = $this->organizationRepository->delete($organization);

        return back()->withFlashSuccess('Organization removed successfully!');
    }
}
