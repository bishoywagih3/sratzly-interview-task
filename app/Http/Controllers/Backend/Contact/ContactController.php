<?php

namespace App\Http\Controllers\Backend\Contact;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Contact\StoreContactRequest;
use App\Http\Requests\Backend\Contact\UpdateContactRequest;
use App\Models\Contact\Contact;
use App\Models\Organization\Organization;
use App\Repositories\Backend\ContactRepository;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * @var ContactRepository
     */
    private $contactRepository;

    public function __construct(ContactRepository $contactRepository)
    {
        $this->contactRepository = $contactRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return View
     */
    public function index(Request $request)
    {
        $contacts = $this->contactRepository->getPaginated($request->all(), 10);

        return view('contacts.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $organizations = Organization::all();

        return view('contacts.create', [
            'organizations' => $organizations
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreContactRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreContactRequest $request)
    {
        $contact = $this->contactRepository->store($request->all());

        return response()->json($contact, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Contact\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        $organizations = Organization::all();

        return view('contacts.edit', [
            'contact' => $contact->load('mobiles', 'organization'),
            'organizations' => $organizations
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contact\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateContactRequest $request, Contact $contact)
    {
        $contact = $this->contactRepository->update($contact, $request->all());

        return response()->json($contact, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contact\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        $this->contactRepository->delete($contact);

        return back()->withFlashSuccess('Contact Deleted Successfully!');
    }

    public function checkEmailIsUnique(string $email, int $id)
    {
        $contact = Contact::where('email', $email)->first();
        if(! $contact){
            return response()->json('valid', 200);
        }

        if($id !== 0){
            $contactById = Contact::find($id);
            if($contactById->email === $email){
                return response()->json('valid', 200);
            }
            else{
                return response()->json('invalid', 422);
            }
        }

        return response()->json('invalid', 422);
    }

    public function emailVerification(string $token)
    {
        $contact = Contact::where('email_verification_token', $token)->first();

        if($contact){
            $contact->email_verified_at = Carbon::now();
            $contact->email_verification_token = null;
            $contact->save();
        }

        return view('contacts.email_verified');
    }
}
