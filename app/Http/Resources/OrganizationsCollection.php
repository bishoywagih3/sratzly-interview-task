<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OrganizationsCollection extends ResourceCollection
{
    private $pagination;

    public function __construct($resource)
    {
        $this->pagination = [
            'total' => $resource->total(),
            'count' => $resource->count(),
            'per_page' => $resource->perPage(),
            'current_page' => $resource->currentPage(),
            'total_pages' => $resource->lastPage()
        ];

        $resource = $resource->getCollection();

        parent::__construct($resource);
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function($organization){
                return [
                    'id'            => $organization->id,
                    'name'          => $organization->name,
                    'category'      => $organization->category,
                    'trade_license' => $organization->trade_license,
                    'logo'          => $organization->organizationLogo,
                    'licensed_date' => $organization->licensed_date,
                    'created_at'    => $organization->created_at,
                    'updated_at'    => $organization->updated_at,
                ];
            }),
            'pagination' => $this->pagination
        ];
    }
}
