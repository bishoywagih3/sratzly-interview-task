<?php

namespace App\Models\ContactMobile;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactMobile extends Model
{
    use HasFactory;

    protected $table = 'contact_mobiles';

    protected $guarded = [];
}
