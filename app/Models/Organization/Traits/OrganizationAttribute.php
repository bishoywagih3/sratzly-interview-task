<?php

namespace App\Models\Organization\Traits;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

trait OrganizationAttribute
{
    public function getOrganizationLogoAttribute()
    {
        $path = Storage::url('organizations/' . $this->logo);

        if (! Storage::exists('/organizations/' . $this->logo) || is_null($this->logo)) {
            $path = asset('assets/images/organization.svg');
        }

        return $path;
    }
}
