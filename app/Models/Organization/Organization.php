<?php

namespace App\Models\Organization;

use App\Models\Organization\Traits\OrganizationAttribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    use HasFactory,
        OrganizationAttribute;

    protected $guarded = [];

    protected $dates = ['licensed_date'];

    protected $appends = ['organizationLogo'];
}
