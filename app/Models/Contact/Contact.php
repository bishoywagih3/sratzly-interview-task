<?php

namespace App\Models\Contact;

use App\Models\Contact\Traits\ContactRelationship;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Contact extends Model
{
    use HasFactory,
        ContactRelationship,
        Notifiable;


    protected $guarded = [];

    protected $dates = ['birth_date', 'email_verified_at'];
}
