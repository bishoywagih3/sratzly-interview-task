<?php

namespace App\Models\Contact\Traits;

use App\Models\ContactMobile\ContactMobile;
use App\Models\Organization\Organization;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait ContactRelationship
{
    public function organization(): BelongsTo
    {
        return $this->belongsTo(Organization::class);
    }

    public function mobiles()
    {
        return $this->hasMany(ContactMobile::class, 'contact_id');
    }
}
