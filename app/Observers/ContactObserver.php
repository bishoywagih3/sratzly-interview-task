<?php

namespace App\Observers;

use App\Models\Contact\Contact;
use App\Notifications\ContactEmailVerification;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class ContactObserver
{
    /**
     * Handle the Contact "created" event.
     *
     * @param  \App\Models\Contact\Contact  $contact
     * @return ContactEmailVerification
     */
    public function created(Contact $contact)
    {
        $contact->email_verification_token = Str::random(20);
        $contact->save();

        Notification::send($contact, new ContactEmailVerification($contact));
    }
}
