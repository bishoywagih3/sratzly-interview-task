<?php

namespace App\Repositories\Backend;

use App\Models\Organization\Organization;
use Faker\Factory;
use Illuminate\Support\Facades\File;

class OrganizationRepository
{
    /**
     * @var Organization
     */
    private $model;

    public function __construct(Organization $model)
    {
        $this->model = $model;
    }

    public function getPaginated($rows = 25, $order = 'id', $sort = 'asc')
    {
        return $this->model->
                    orderBy($order, $sort)
                    ->paginate($rows);
    }

    public function store(array $data)
    {
        return $this->model::create($data);
    }

    public function update(Organization $organization, array $data)
    {
        if(isset($data['logo'])) {
            $this->deleteImageFromServer($organization);
        }

        return tap($organization)->update($data);
    }

    public function delete(Organization $organization)
    {
        $this->deleteImageFromServer($organization);

        return $organization->delete();
    }

    protected function deleteImageFromServer(Organization $organization)
    {
        if($organization->logo || File::exists( storage_path('app/public/organizations/' . $organization->logo))){
            File::delete( storage_path('app/public/organizations/' . $organization->logo));
        }
    }
}
