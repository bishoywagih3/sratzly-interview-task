<?php

namespace App\Repositories\Backend;

use App\Models\Contact\Contact;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ContactRepository
{
    /**
     * @var Contact
     */
    private $model;

    public function __construct(Contact $model)
    {
        $this->model = $model;
    }

    public function getPaginated(array $filters, $rows = 25, $order = 'id', $sort = 'asc')
    {
        $contacts = $this->model;

        if(isset($filters['query'])){
            $query = $filters['query'];
            $contacts = $contacts->where('first_name', 'LIKE', "%$query%")
                                    ->orWhere('last_name' ,'LIKE', "%$query%")
                                    ->orWhere('email' ,'LIKE', "%$query%")
                                    ->orWhereHas('mobiles', function ($mobileQuery) use ($query) {
                                        $mobileQuery->where('number', 'LIKE', "%$query%");
                                    });
        }

        if(isset($filters['is_valid'])){
            if($filters['is_valid'] !== '0') {
                $validity = $filters['is_valid'];
                if ($validity === 'valid') {
                    $contacts = $contacts->whereNotNull('email_verified_at');
                } else {
                    $contacts = $contacts->whereNull('email_verified_at');
                }
            }
        }

        return $contacts
                        ->orderBy($order, $sort)
                        ->paginate($rows);
    }

    public function store(array $data)
    {
        return DB::transaction(function () use ($data) {

            $contact = $this->model::create([
                'first_name'    => $data['firstName'],
                'last_name'     => $data['lastName'],
                'email'         => $data['email'],
                'organization_id'  => $data['organization']['id'],
                'birth_date'    => $data['birthDate']
            ]);

            if($contact){
                foreach ($data['mobiles'] as $mobile) {
                    $contact->mobiles()->create([
                        'number' => $mobile['number']
                    ]);
                }
            }
        });

    }

    public function update(Contact $contact, array $data)
    {
        return DB::transaction(function () use ($data, $contact) {
            $contact = tap($contact)->update([
                'first_name'        => $data['firstName'],
                'last_name'         => $data['lastName'],
                'email'             => $data['email'],
                'organization_id'   => $data['organization']['id'],
                'birth_date'        => $data['birthDate']
            ]);

            if($contact){
                $contact->mobiles()->delete();
                foreach ($data['mobiles'] as $mobile) {
                    $contact->mobiles()->create([
                        'number' => $mobile['number']
                    ]);
                }
            }
        });
    }

    public function delete(Contact $contact)
    {
        $contact->mobiles()->delete();

        return $contact->delete();
    }

}
