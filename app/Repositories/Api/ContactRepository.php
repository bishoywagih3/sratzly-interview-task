<?php


namespace App\Repositories\Api;


use App\Models\Contact\Contact;
use Illuminate\Support\Collection;

class ContactRepository
{
    /**
     * @var Contact
     */
    private $model;

    public function __construct(Contact $model)
    {
        $this->model = $model;
    }

    public function getPaginated(array $requestData)
    {
        return $this->getSorted($requestData);
    }

    protected function getSorted(array $requestData, $contacts = null)
    {
        $orderBy = isset($requestData['orderBy']) ? $requestData['orderBy'] : 'id';
        $sort = isset($requestData['sort']) ? $requestData['sort'] : 'asc';
        $perPage = isset($requestData['perPage']) ? $requestData['perPage'] : '25';

        $contacts = $contacts ? $contacts : $this->model;

        return $contacts
                    ->orderBy($orderBy, $sort)
                    ->paginate($perPage);
    }

    public function getFilteredWithOrganization(array $requestData)
    {
        $contacts = $this->model;

        if(isset($requestData['organization_id'])){
            $contacts = $contacts->where('organization_id', $requestData['organization_id']);

            return $this->getSorted($requestData, $contacts);
        }
    }

    public function getFilteredWithEmail(array $requestData)
    {
        $contacts = $this->model;

        if(isset($requestData['email'])){
            $email = $requestData['email'];
            $contacts = $contacts->where('email', 'LIKE' , "%$email%");
        }

        if(isset($requestData['is_verified'])){
            $isVerified = $requestData['is_verified'];

            if($isVerified === 'true'){
                $contacts = $contacts->whereNotNull('email_verified_at');
            }
            else{
                $contacts = $contacts->whereNull('email_verified_at');
            }
        }

        return $this->getSorted($requestData, $contacts);
    }
}
