<?php

namespace App\Repositories\Api;

use App\Models\Organization\Organization;

class OrganizationRepository
{
    /**
     * @var Organization
     */
    private $model;

    public function __construct(Organization $model)
    {
        $this->model = $model;
    }

    public function getPaginated(array $filterData)
    {
        $orderBy = isset($filterData['orderBy']) ? $filterData['orderBy'] : 'id';
        $sort = isset($filterData['sort']) ? $filterData['sort'] : 'asc';
        $perPage = isset($filterData['perPage']) ? $filterData['perPage'] : '25';

        return $this->model
                    ->orderBy($orderBy, $sort)
                    ->paginate($perPage);
    }
}
