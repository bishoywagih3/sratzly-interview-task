@component('mail::message')
# Thanks for Choosing Starzly

Please verify your email address from the below link

@component('mail::button', ['url' => config('app.url').'/contacts/email-verification/' . $contact->email_verification_token])
    Verify Email
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
