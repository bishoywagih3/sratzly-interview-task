@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="mb-2 float-right">
                    <a href="{{ route('contacts.create') }}" class="btn btn-primary">Add Contact</a>
                </div>

                @include('contacts.filter')

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">First Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">Organization</th>
                        <th scope="col">Email</th>
                        <th scope="col">Is Verified?</th>
                        <th scope="col">Last Updated</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($contacts as $contact)
                        <tr>
                            <td>{{ $contact->first_name }}</td>
                            <td>{{ $contact->last_name }}</td>
                            <td>{{ optional($contact->organization)->name }}</td>
                            <td>{{ $contact->email }}</td>
                            <td>{{ $contact->email_verified_at ? 'yes' : 'no' }}</td>
                            <td>{{ $contact->updated_at->diffForHumans() }}</td>
                            <td>
                                <div style="display: flex; justify-content: center; align-items: center">
                                    <a href="{{ route('contacts.edit', $contact->id) }}" class="btn btn-primary">Edit</a>
                                    <form class="ml-1" method="post" action="{{ route('contacts.destroy', $contact->id) }}">
                                        @method('delete')
                                        @csrf
                                        <button class="btn btn-danger delete-row" type="submit">Delete</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="text-center">
                    @if($contacts->count() === 0)
                        <h4>No Data Found!</h4>
                    @endif
                </div>

                {{ $contacts->render() }}


            </div>
        </div>
    </div>
@endsection
