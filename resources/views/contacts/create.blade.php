@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Create Contact</div>
                    <div class="card-body">
                        <create-contact :organizations="{{ $organizations }}" />
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
