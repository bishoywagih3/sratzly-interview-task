@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Edit Contact</div>
                    <div class="card-body">
                        <edit-contact
                            :contact="{{ $contact }}"
                            :organizations="{{ $organizations }}"
                        ></edit-contact>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
