<form method="get" action="{{ route('contacts.index') }}">
    @csrf
    <div class="mb-4">
        <div class="row" style="padding-top: 10px">
            <div class="col-12 col-md-12 ">
                <div class="row" style="padding-top: 10px">
{{--                    <div class="col-md-1">Search</div>--}}
                    <div class="col-md-3">
                        <input type="text" value="{{ request()->get('query') }}" name="query" class="form-control" placeholder="Search by name, email or mobile">
                    </div>

{{--                    <div class="col-md-1">Search..</div>--}}
                    <div class="col-md-3">
                        <select name="is_valid" class="form-control" id="is_valid">
                            <option value="0">Email Status</option>
                            <option value="valid" {{ request()->get('is_valid') === 'valid' ? 'selected' : '' }}>Validated</option>
                            <option value="invalid" {{ request()->get('is_valid') === 'invalid' ? 'selected' : '' }}>not validated</option>
                        </select>
                    </div>

                    <div class="filter-btns col-md-4">
                        <button type="submit" id="" class="btn btn-success btn-w-m btn-success btn-rounded">Search</button>
                        <a href="{{ route('contacts.index') }}" class="btn btn-success btn-w-m btn-danger btn-rounded">cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
