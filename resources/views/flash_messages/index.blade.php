<div class="container">
    @if(session()->has('flash_success'))
        @php
            $message = session('flash_success');
        @endphp
        <x-alert type="success" :message="$message"></x-alert>
    @endif

    @if(session()->has('flash_error'))
        @php
            $message = session('flash_error');
        @endphp
        <x-alert type="danger" :message="$message"></x-alert>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

</div>
