@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="mb-2 float-right">
                    <a href="{{ route('organizations.create') }}" class="btn btn-primary">Add Organization</a>
                </div>

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Category</th>
                        <th scope="col">Trade License</th>
                        <th scope="col">Licensed Date</th>
                        <th scope="col">Last Updated</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($organizations as $organization)
                        <tr>
                            <td>{{ $organization->name }}</td>
                            <td>{{ $organization->category }}</td>
                            <td>{{ $organization->trade_license }}</td>
                            <td>{{ $organization->licensed_date->format('Y-m-d') }}</td>
                            <td>{{ $organization->updated_at->diffForHumans() }}</td>
                            <td>
                                <div style="display: flex; justify-content: center; align-items: center">
                                    <a href="{{ route('organizations.edit', $organization->id) }}" class="btn btn-primary">Edit</a>
                                    <form class="ml-1" method="post" action="{{ route('organizations.destroy', $organization->id) }}">
                                        @method('delete')
                                        @csrf
                                        <button class="btn btn-danger delete-row" type="submit">Delete</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $organizations->render() }}
            </div>
        </div>
    </div>
@endsection
