<div class="form-group">
    {!! Form::label('name', 'Name') !!}
    <span class="text-danger">*</span>
    {!! Form::text('name', null, [ 'class' => ($errors->has('name')) ? 'form-control is-invalid' : 'form-control' , 'id' => 'name', 'placeholder' => 'Organization Name']); !!}
</div>
<div class="form-group">
    {!! Form::label('category', 'Category'); !!}
    <span class="text-danger">*</span>
    {!! Form::select('category', $categories, null , ['class' => ($errors->has('category')) ? 'form-control is-invalid' : 'form-control', 'id' => 'category']); !!}
</div>
<div class="form-group">
    {!! Form::label('trade_license', 'Trade License'); !!}
    {!! Form::text('trade_license', null , ['class' => 'form-control', 'id' => 'trade_license', 'placeholder' => 'Trade License']); !!}
</div>

<div class="form-group">
    {!! Form::label('licensed_date', 'Licensed Date'); !!}
    {!! Form::date('licensed_date', isset($organization) ? $organization->licensed_date : null , ['class' => 'form-control', 'id' => 'trade_license']); !!}
</div>

@isset($organization)
    <div class="form-group">
        <img width="150px" height="150px" src="{{ $organization->organizationLogo }}" alt="{{ $organization->name }}">
    </div>
@endisset

<div class="form-group">
    {!! Form::label('file', 'Logo'); !!}
    <input type="file" name="file">
</div>
