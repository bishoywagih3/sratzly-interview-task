@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Create Organization</div>
                <div class="card-body">
                    {!! Form::open(['route' => 'organizations.store', 'files' => true]) !!}
                    @include('organizations.form')
                    <button type="submit" class="btn btn-primary">Submit</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
