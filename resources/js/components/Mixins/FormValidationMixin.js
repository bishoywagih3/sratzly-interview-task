import moment from "moment";
export default {
    data(){
      return {
          errors: [],
          requiredMessage: 'This field is required',
          invalidEmailMessage: 'Please enter a valid email',
          invalidMobile: 'Mobile number must be 11 number',
          emailTakenMessage: "This email is already exists",
      }
    },
    methods: {
        checkEmailIsUnique()
        {
          let email = this.form.email.trim();
          if(this.isValidEmail( email )) {
              axios.get('/contacts/check-email-is-unique/' + email + '/' + +this.form.id)
                  .then(response => {
                      let index = this.errors.findIndex(error => {
                         return error === 'email_taken';
                      });
                      this.errors.splice(index, 1);
                  }).catch(errors => {
                  this.errors.push('email_taken');
              });
          }
        },
        formValidation(){
            if(! this.errors.includes('email_taken')){
                this.errors = [];
            }

            if(! this.form.firstName.trim()){
                this.errors.push('firstName_required');
            }
            if(! this.form.lastName.trim()){
                this.errors.push('lastName_required');
            }
            if(! this.form.email.trim()){
                this.errors.push('email_required');
            }
            else if(! this.isValidEmail(this.form.email.trim()) ){
                this.errors.push('email_notValid');
            }

            if(! this.form.organization){
                this.errors.push('organization_required');
            }

            this.form.mobiles.map(mobile => {
                if(! mobile.number.trim()){
                    this.errors.push('mobile_required_' + mobile.id)
                }
                else if(! this.isValidMobile(mobile.number.trim())){
                    this.errors.push('mobile_invalid_' + mobile.id)
                }
            });

            const date = moment(this.form.birthDate.trim());
            if(! date.isValid()){
                this.errors.push('birthDate_required');
            }

            return this.errors.length === 0;
        },

        isValidEmail(email) {
            const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },

        isValidMobile(mobile) {
            return mobile.length === 11 && ! isNaN(mobile);
        },

    }
}
