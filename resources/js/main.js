import Swal from 'sweetalert2'

$(document).ready(function (){
    $('.delete-row').click(function (e){
        const row = $(this);
        e.preventDefault();
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                row.closest('form').submit();
            }
        })
    });
});
