## Starzly Interview Test

### How to Set-up The project.

- clone the public repo.
- cp .env.example .env
- composer install
- npm install
- npm run dev
- php artisan storage:link
- add email parameters to .env
- php artisan migrate --seed
- php artisan jwt:secret

### Notes about the Project.

- organizations CRUD build by blade templates.
- Contact create and edit Form are build and validated by VueJs.
- Created Content notification email is sent via Contact Observer "created event".
- All Apis Authenticated with JWT.
- Application has default Admin Seeder for easily generate Token and test the Apis.
- Application support registering new Admins for Authenticating Apis.   
