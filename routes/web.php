<?php

use App\Http\Controllers\Backend\Contact\ContactController;
use App\Http\Controllers\Backend\Organization\OrganizationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('organizations', OrganizationController::class);

Route::get('/contacts/{token}/email-verification', [ContactController::class, 'emailVerification'])->name('contacts.email-verification');
Route::get('/contacts/check-email-is-unique/{email}/{id}', [ContactController::class, 'checkEmailIsUnique']);
Route::resource('contacts', ContactController::class);
