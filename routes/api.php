<?php

use App\Http\Controllers\Api\ContactController;
use App\Http\Controllers\Api\OrganizationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [\App\Http\Controllers\Api\AuthController::class, 'login']);
Route::post('register', [\App\Http\Controllers\Api\AuthController::class, 'register']);

Route::middleware('jwt.verify')->group(function (){
    Route::get('organizations', [ OrganizationController::class, 'index' ]);
    Route::get('contacts/filter-by-organization', [ ContactController::class, 'getFilteredWithOrganization' ]);
    Route::get('contacts/filter-by-email', [ ContactController::class, 'getFilteredWithEmail' ]);
    Route::get('contacts', [ ContactController::class, 'index' ]);
});
